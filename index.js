/*3. Create a trainer object using object literals.
4. Initialize/add the following trainer object properties:
- Name (String)
- Age (Number)
- Pokemon (Array)
- Friends (Object with Array values for properties)*/

let trainer = {
	name: 'Ash Ketcuhm',
	age: '10',
	friends: {
		hoenn: ['May', 'Max'],
		kanto: ['Brock','Misty'],
	},
	pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
	talk: function(){
		console.log('Pickachu! I choose you');
	}

} 
console.log(trainer);
console.log(typeof trainer);

/*5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!*/
/*6. Access the trainer object properties using dot and square bracket notation.*/

console.log('Result of dot notation:');
console.log(trainer.name);

console.log('Result of square bracket notation:');
console.log(trainer['pokemon']);

console.log('Result of talk method:');
trainer.talk();

/*7. Invoke/call the trainer talk object method.
8. Create a constructor for creating a pokemon with the following properties:
- Name (Provided as an argument to the contructor)
- Level (Provided as an argument to the contructor)
- Health (Create an equation that uses the level property)
- Attack (Create an equation that uses the level property)*/

function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;
	
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		x = (target.health - this.attack)
		console.log(target.name + "'s health is now reduced to " + x);

		if(target.health - this.attack){
			target.health = x
		}

		if (x <= 0){
			target.faint();
		}
	}
	this.faint = function (){
		console.log(this.name + ' fainted');
	}

}
let pikachu = new Pokemon('Pikachu', 12);
console.log(pikachu);

let geodude = new Pokemon('Geodude', 8);
console.log(geodude);

let meotwo = new Pokemon('Meotwo', 100);
console.log(meotwo);


/*10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
11. Create a faint method that will print out a message of targetPokemon has fainted.
12. Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
13. Invoke the tackle method of one pokemon object to see if it works as intended.*/

geodude.tackle(pikachu);

meotwo.tackle(geodude);

